'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const browserSync = require('browser-sync');



gulp.task('sass', () => {
    return gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css/'));

});


gulp.task('browser-sync', function () {
    var files = ['./*.html', './css/*.scss', './css/*.css', './img/*.{png, jpg, gif}', '/js/*.js'];
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    })
});

gulp.task('sass:watch', function () {
    gulp.watch('./css/*.scss', ['sass']);
});

gulp.task('default', gulp.series('browser-sync', 'sass:watch'));